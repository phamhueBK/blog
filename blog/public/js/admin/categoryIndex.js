function btnAddCategory(){
    $('#addCategory').modal('show');
}
function btnCreateCategory(){
    var name = $('#addName').val();
    var parent_id = $('#add_parent_id').val();
    $.ajax({
        type: 'post',
        url: 'addCategory',
        data: {
            '_token': $('input[name=_token]').val(),
            'name': name,
            'parent_id': parent_id,
            'slug': name,
        },
        success: function(data) {
            console.log(data);
            var parent = "";
            if(data.parent_id != 0)
                parent = data.parent.name;
            var html = '<tr id="category_'+data.id+'"><td id="name_"'+data.id+'>'+data.name+'</td><td></td><td id="parent_'+data.id+'">'+parent+'</td><td><a onclick="btnEditCategory('+data.slug+')" class="btn btn-success">Edit</a><a onclick="btnDeleteCategory('+data.id+')" class="btn btn-danger">Delete</a></td>';  
            
            $('#menu').append(html);

            $('#addCategory').modal('hide');
        }
    });
    //alert("HELLO");
}
function btnEditCategory(id){
    $('#editCategory').modal('show');
    $.ajax({
        type: 'get',
        url: 'findCategory?title='+id,
        data: {
            
        },
        success: function(data) {
            console.log(data);
            $('#editName').val(data.name);
            $('#editId').val(data.id);
            $('#edit_parent_id').val(data.parent_id);

        }
    });
}

function btnUpdateCategory(){
    var name = $('#editName').val();
    var id = $('#editId').val();
    var edit_parent_id = $('#edit_parent_id').val();
    $.ajax({
        type: 'post',
        url: 'update',
        data: {
            '_token': $('input[name=_token]').val(),
            'id': id,
            'name': name,
            'parent_id': edit_parent_id,
        },
        success: function(data) {
            console.log(data);

            var html = '<td id="name_'+data.id+'">'+data.name+'</td>';
            
            document.getElementById('name_'+data.id).innerHTML = data.name;
            if(data.parent_id != 0)
                document.getElementById('parent_'+data.id).innerHTML = data.parent.name;
            else
                document.getElementById('parent_'+data.id).innerHTML = "";
            console.log($('#name_'+data.id).val());
            $('#editCategory').modal('hide');
        }
    });
}

function btnDeleteCategory(id){
    /*$('#deleteId').val(id);
    $('#deleteCategory').modal('show');*/
    swal(
        {
            title: "Bạn có chắc muốn xóa?",
            // text: "Bạn sẽ không thể khôi phục lại bản ghi này!!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "Không",
            confirmButtonText: "Có",
            // closeOnConfirm: false,
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: 'deleteCategory',
                    data : {
                        '_token': $('input[name=_token]').val(),
                        'id': id,
                    },
                    success: function(res)
                    {
                        console.log(res);
                        if(!res.error) {
                            toastr.success('Xóa thành công!');
                            $('#category_'+id).remove();
                      
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        toastr.error(thrownError);
                    }
                });
            } else {
                toastr.info("Thao tác xóa đã bị huỷ bỏ!");
            }
        }
    );
}

function DeleteCategory(){
    $.ajax({
        type: 'post',
        url: 'deleteCategory',
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('#deleteId').val(),
        },
        success: function(id) {
            console.log(id);
            $('#category_'+id).remove();
            $('#deleteCategory').modal('hide');
        }
    });
}
$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
