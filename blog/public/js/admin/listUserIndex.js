function btnShow(id){
	
	if($('#danhDau').val() == "hide"){
		//alert("Đúng rồi");
		$("#info_"+id).toggle();
		$('#show_'+id).addClass('fa-minus-circle');
	    $('#show_'+id).removeClass('fa-plus-circle');
	    $('#danhDau').val("more");
	}
	else{
		$("#info_"+id).toggle();
		//alert("Hide");
    	$('#show_'+id).addClass('fa-plus-circle');
    	$('#show_'+id).removeClass('fa-minus-circle');
    	$('#danhDau').val("hide");
	}
}


function btnApprove(id){
    swal(
        {
            title: "Bạn muốn mở khóa tài khoản này?",
            // text: "Bạn sẽ không thể khôi phục lại bản ghi này!!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "Không",
            confirmButtonText: "Đúng",
            // closeOnConfirm: false,
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: 'deleteUser',
                    data : {
                        '_token': $('input[name=_token]').val(),
                        'id': id,
                        'status': 1,
                    },
                    success: function(data)
                    {
                        console.log(data);
                        if(!data.error) {
                            toastr.success('Mở khóa thành công!');
                            $('#repeat_'+data.id).removeClass('fa-repeat');
                            $('#repeat_'+data.id).removeClass('fa');
                            $('#repeat_'+data.id).addClass('glyphicon');
                            $('#repeat_'+data.id).addClass('glyphicon-remove');
                            /*$('#repeat_'+data.id).hide();
                            $('#delete_'+data.id).show();
                            document.getElementById('delete_'+data.id).style.display = 'block';*/
                            document.getElementById('status_'+data.id).innerHTML = "Đang hoạt động";
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        toastr.error(thrownError);
                    }
                });
            } else {
                toastr.info("Thao tác xóa đã bị huỷ bỏ!");
            }
        }
    );
}
function btnDelete(id){
    swal(
        {
            title: "Bạn có chắc muốn xóa?",
            // text: "Bạn sẽ không thể khôi phục lại bản ghi này!!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "Không",
            confirmButtonText: "Có",
            // closeOnConfirm: false,
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: 'deleteUser',
                    data : {
                        '_token': $('input[name=_token]').val(),
                        'id': id,
                        'status': 0,
                    },
                    success: function(data)
                    {
                        console.log(data);
                        if(!data.error) {
                            toastr.success('Xóa thành công!');
                            $('#delete_'+data.id).addClass('fa-repeat');
                            $('#delete_'+data.id).addClass('fa');
                            $('#delete_'+data.id).removeClass('glyphicon');
                            $('#delete_'+data.id).removeClass('glyphicon-remove');
                            /*$('#delete_'+data.id).hide();
                            $('#repeat_'+data.id).show();
                            document.getElementById('repeat_'+data.id).style.display = 'block';*/
                            document.getElementById('status_'+data.id).innerHTML = "Không hoạt động";
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        toastr.error(thrownError);
                    }
                });
            } else {
                toastr.info("Thao tác xóa đã bị huỷ bỏ!");
            }
        }
    );
}
function DeleteUser(){
	var status = 0;
    if($('#status').val() == 0)
    	status = 1;
    $.ajax({
        type: 'post',
        url: 'deleteUser',
        data: {
            '_token': $('input[name=_token]').val(),
            'id': $('#deleteId').val(),
            'status': status,
        },
        success: function(data) {
            console.log(data);
            if(data.status == 0){
            	$('#delete_'+data.id).addClass('fa-repeat');
	   			$('#delete_'+data.id).removeClass('fa-remove');
	   			document.getElementById('status_'+data.id).innerHTML = "Không hoạt động";
	   			toastr.success('Tài khoản '+data.name+' đã bị khóa!', 'Nafosted',{timeOut: 1000});
            }
            else{
            	$('#repeat_'+data.id).addClass('fa-remove');
	   			$('#repeat_'+data.id).removeClass('fa-repeat');
	   			document.getElementById('status_'+data.id).innerHTML = "Đang hoạt động";
	   			toastr.success('Tài khoản '+data.name+' đã được mở!', 'Nafosted',{timeOut: 1000});
            }
            
            $('#deletePost').modal('hide');
            
        }
    });
}
function btnCreateUser(){
    $('#createUser').modal('show');
}
function btnSaveCreateUser(){
    var name = $('#addName').val();
    var email = $('#addEmail').val();
    var password = $('#addPassword').val();

    $.ajax({
        type: 'post',
        url: 'create',
        data: {
            '_token': $('input[name=_token]').val(),
            'name': name,
            'email': email,
            'password': password,
        },
        success: function(data) {
            console.log(data);
            var html = '<tr id="user_'+data.id+'"><td>'+data.name+'</td><td></td><td>'+data.email+'</td><td></td><td><a onclick="btnShow('+data.id+')" class="btn btn-success"><i class="fa fa-plus-circle" id="show_'+data.id+'" aria-hidden="true"></i></a><a onclick="btnEdit('+data.id+')" class="btn btn-success"><span class="glyphicon glyphicon-edit" id="show_'+data.id+'"></span></a><a onclick="btnDelete('+data.id+')" class="btn btn-success"><i id="delete_'+data.id+'" class="glyphicon glyphicon-remove" aria-hidden="true"></i></a></td></tr><tr style="display: none" id="info_'+data.id+'"><td colspan="5"><input type="hidden" id="danhDau" value="hide"><b>Posts:</b> 0<br><b>Mobile:</b><br><b>Birthday:</b><br><b>Created_at:</b> '+data.created_at+'<br><b>Updated_at:</b> '+data.updated_at+'<br><b>Status:</b> <span id="status_'+data.id+'">Đang hoạt động</span><br><b>Avatar:</b> <br></td></tr>';
            $(html).insertAfter('#menu');
            $('#createUser').modal('hide');
            toastr.success('Tạo người dùng thành công!', 'Nafosted',{timeOut: 1000});
        }
    });
}
function btnEdit(id){
    $('#editUser').modal('show');
    $.ajax({
        type: 'get',
        url: 'findUser?id='+id,
        data: {
            
        },
        success: function(data) {
            console.log(data);
            $('#editName').val(data.name);
            $('#editId').val(data.id);
            $('#editEmail').val(data.email);
        }
    });
}

function btnSaveEditUser(){
    var id = $('#editId').val();
    var name = $('#editName').val();
    var email = $('#editEmail').val();
    var password = $('#editPassword').val();

    $.ajax({
        type: 'post',
        url: 'update',
        data: {
            '_token': $('input[name=_token]').val(),
            'id': id,
            'name': name,
            'email': email,
            'password': password,
        },
        success: function(data) {
            console.log(data);
            var html = '<td>'+data.name+'</td><td></td><td>'+data.email+'</td><td></td><td><a onclick="btnShow('+data.id+')" class="btn btn-success"><i class="fa fa-plus-circle" id="show_'+data.id+'" aria-hidden="true"></i></a><a onclick="btnEdit('+data.id+')" class="btn btn-success"><span class="glyphicon glyphicon-edit" id="show_'+data.id+'"></span></a><a onclick="btnDelete('+data.id+')" class="btn btn-success"><i id="delete_'+data.id+'" class="glyphicon glyphicon-remove" aria-hidden="true"></i></a></td>';
            $('#user_'+data.id).html(html);
            $('#editUser').modal('hide');
        }
    });
}
