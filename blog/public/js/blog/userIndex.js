function btnAddPost(){
    $('#addPost').modal('show');
}

function btnShow(id){
    $('#showUser').modal('show');
}
function SaveNewPost(){
    
    var title = $('#addTitle').val();
    var description = $('#addDescription').val();
    //var content = $('#addContent').val();
    var content = CKEDITOR.instances.addContent.getData();
    var category_id = $('#addType').val();
    var thumbnail = $('#addThumbnail').val();
    var tagArr = $("#taginput").val();
    var user_id = $('#addUserId').val();

    $.ajax({
        type: 'post',
        url: 'addPost',
        data: {
            '_token': $('input[name=_token]').val(),
            'title': title,
            'description': description,
            'content': content,
            'thumbnail': thumbnail,
            'category_id': category_id,
            'views': 0,
            'user_id': user_id,
            'tag': tagArr,
        },
        success: function(data) {
            console.log(data);
            var html1 = '<div class="about-one"><a href="/find_post_by_category?category='+data.category.slug+'" class="btn btn-primary">'+data.category.name+'</a><h3>'+data.title+'</h3></div><div class="about-two"><a href="/post/detail?title='+data.slug+')}}"><img src="'+data.thumbnail+'" alt="" /></a><br><p>Posted by <a href="#">'+data.author+'</a> on '+data.created_at+' <a href="#">comments(2)</a></p><br>Trạng thái: chờ duyệt<br><div>'+data.description+'</div><div class="about-btn"><a href="post/detail?title='+data.slug+')}}">Read More</a></div><ul><li><p>Share : </p></li><li><a href="#"><span class="fb"> </span></a></li><li><a href="#"><span class="twit"> </span></a></li><li><a href="#"><span class="pin"> </span></a></li><li><a href="#"><span class="rss"> </span></a></li><li><a href="#"><span class="drbl"> </span></a></li></ul></div>';
            $('#list_post').append(html1);
            console.log(html1)
            $('#addPost').modal('hide');
            var html = '<tr id="post_'+data.id+'"><td>'+data.title+'</td><td>'+data.description+'</td><td>'+type+'</td><td><img src="'+data.thumbnail+'" width="100px" /></td><td>'+$('#author').val()+'</td><td>'+data.views+'</td><td>'+data.created_at+'</td><td><a href="http://phamhue.dev:8190/admin/post/show/?title='+data.slug+'" class="btn btn-primary" width="100%">Show</a><a onclick="btnEdit(\''+data.slug+'\')" class="btn btn-success" width="100%">Edit</a><a onclick="btnDelete('+data.id+')" class="btn btn-danger" width="100%">Delete</a></td></tr>';  
            $('#menu').append(html);
            toastr.success('Đăng bài viết thành công!', 'Nafosted',{timeOut: 1000});
        }
    });
}

function btnEdit(id){
    $('#editProfile').modal('show');
    
}

function saveProfile(){
    var name = $('#name').val();
    var email = $('#email').val();
    var mobile = $('#mobile').val();
    var address = $('#address').val();
    var birthday = $('#birthday').val();
    var avatar = $('#avatar').val();
    $.ajax({
        type: 'post',
        url: 'update',
        data: {
            '_token': $('input[name=_token]').val(),
            'name': name,
            'email': email,
            'mobile': mobile,
            'address': address,
            'birthday': birthday,
            'avatar': avatar,
        },
        success: function(data) {
            console.log(data);
            document.getElementById('user_address').innerHTML = 'Address: '+data.address;
            document.getElementById('avatar_main').innerHTML = '<img width="100%" src="'+data.avatar+'"/>';
            document.getElementById('user_mobile').innerHTML = 'Mobile: '+data.mobile;
            document.getElementById('user_birthday').innerHTML = 'Birthday: '+data.birthday;
            document.getElementById('user_name').innerHTML = data.name;
            $('#editProfile').modal('hide');
            toastr.success('Cập nhật thông tin thành công!', 'Nafosted',{timeOut: 1000});
            
        }
    });
}

function upload_thumbnail(){
    var file_data = $('#img').prop('files')[0];
    //lấy ra kiểu file
    var type = file_data.type;
    //console.log(type);
    //Xét kiểu file được upload
    var match= ["image/gif","image/png","image/jpg", "image/jpeg"];
    var form_data = new FormData();
        
    //kiểm tra kiểu file
    if(type == match[0] || type == match[1] || type == match[2] || type == match[3])
    {
        //khởi tạo đối tượng form data
        //thêm files vào trong form data
        form_data.append('file', file_data);
        //sử dụng ajax post
        $.ajax({
            url: 'upload_img', // gửi đến file upload.php 
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                       
            type: 'post',
            success: function(res){
                var html = "<img src='http://phamhue.dev:8190/"+res+"' width='450px'/>";
                $('#img_before').html(html);
                $('#addThumbnail').val('http://phamhue.dev:8190/'+res);
                console.log(html);
            }
        });
    } else{
        $('.status').text('Chỉ được upload file ảnh');
        //
    }

} 
function uploadEdit(){
    var file_data = $('#fileEdit').prop('files')[0];
    //lấy ra kiểu file
    var type = file_data.type;
    //console.log(type);
    //Xét kiểu file được upload
    var match= ["image/gif","image/png","image/jpg", "image/jpeg"];
    var form_data = new FormData();
        
    //kiểm tra kiểu file
    if(type == match[0] || type == match[1] || type == match[2] || type == match[3])
    {
        //khởi tạo đối tượng form data
        //thêm files vào trong form data
        form_data.append('file', file_data);
        //sử dụng ajax post
        $.ajax({
            url: 'upload_img', // gửi đến file upload.php 
            dataType: 'text',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                       
            type: 'post',
            success: function(res){
                var html = "<img src='http://phamhue.dev:8190/"+res+"' width='450px'/>";
                $('#img_before').html(html);
                $('#avatar').val('http://phamhue.dev:8190/'+res);
                console.log(html);
            }
        });
    } else{
        $('.status').text('Chỉ được upload file ảnh');
        //
    }

} 
$(function(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
});
