@extends('blog/layouts/header_footer')
@section('script_profile')
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-tagsinput.css')}}">
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>
<script type="text/javascript" src="{{asset('js/ckeditor.js')}}"></script>
<script type="text/javascript">
    $(function () {
        CKEDITOR.replace('addContent');
        //CKEDITOR.replace('editContent');
    })
</script>

    <script type="text/javascript" src="{{asset('js/blog/userIndex.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap-tagsinput.js')}}"></script>
@endsection
@section('content')
	<div class="about">
	<div class="container">
        <div class="about-main">

        
            <div class="col-md-8 about-left" id="list_post">
            	@foreach($myPost as $post)
                <div class="about-one">
                	@foreach($myCategory[$post->id] as $category)
                    <a href="{{url('find_post_by_category?category='.$category->slug)}}" class="btn btn-primary">{{$category->name}}</a>
                    @endforeach
                    <h3>{{$post->title}}</h3>
                </div>
                <div class="about-two">
                    <a href="{{url('post/detail?title='.$post->slug)}}"><img src="{{$post->thumbnail}}" alt="" /></a>
                    <br>
                    <p>Posted by <a href="#">{{Auth::user()->name}}</a> on {{$post->created_at}} <a href="#">comments(2)</a></p>
                    <br>
                    @if($post->status == 0)
                    <p style="color: black">Trạng thái: Chờ duyệt</p>
                    @elseif($post->status == 1)
                    <p style="color: black">Trạng thái: Đã duyệt</p>
                    @else
                    <p style="color: black">Trạng thái: Đã xóa</p>
                    @endif
                    <br>
                    <div>{{$post->description}}</div>
                    <div class="about-btn">
                        <a href="{{url('post/detail?title='.$post->slug)}}">Read More</a>
                    </div>
                    <ul>
                        <li><p>Share : </p></li>
                        <li><a href="#"><span class="fb"> </span></a></li>
                        <li><a href="#"><span class="twit"> </span></a></li>
                        <li><a href="#"><span class="pin"> </span></a></li>
                        <li><a href="#"><span class="rss"> </span></a></li>
                        <li><a href="#"><span class="drbl"> </span></a></li>
                    </ul>
                </div>   
                @endforeach
                {{$myPost->links()}}
            </div>


            <div class="col-md-4 about-right heading">
                <div class="abt-1">
                    <h3 id="user_name">{{Auth::user()->name}}</h3>
                    <div class="abt-one">
                        <div id="avatar_main"><img width="100%" height="auto" src="{{Auth::user()->avatar}}" alt="" /></div>
                        <p id="user_address">Address: {{Auth::user()->address}}</p>
	                	<p id="user_mobile">Mobile: {{Auth::user()->mobile}}</p>
	                	<p id="user_birthday">Birthday: {{Auth::user()->birthday}}</p>
                    </div>
                    <div class="about-btn">
                        <a onclick="btnEdit('Auth::user()->id')" class="btn btn-default">Chỉnh sửa</a>
                    </div>
                    <div class="about-btn">
                        <a onclick="btnAddPost()" class="btn btn-primary">Đăng bài</a>
                    </div>
                    <div class="about-btn">
                        <a href="{{url('/user/blog_list')}}" class="btn btn-primary">Danh sách bài đăng</a>
                    </div>
                </div>
                
            </div>
            <div class="clearfix"></div>            
        </div>      
    </div>
</div>
<!-- Modal Edit Profile-->
	<div class="modal fade" id="editProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h5 class="modal-title" id="exampleModalLabel">Edit my profile</h5>
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          		<span aria-hidden="true">&times;</span>
		        	</button>
		      	</div>
		      	<div class="modal-body">
		        	<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
		        		<form action="" method="POST" class="form-horizontal" role="form">
	        		 		<div class="form-group">
        		 				<input type="hidden" name="_token" value="{{csrf_token()}}">
        		 			</div>
        		 			<div class="form-group">
        		 				<label>Name</label>
        		 				<input type="text" class="form-control" id="name" name="name" placeholder="Your name" value="{{Auth::user()->name}}">
        		 			</div>
        		 			<div class="form-group">
								<label>Avatar</label>
								<div id="img_before">
                                    <img src="{{Auth::user()->avatar}}" width="450px">
								</div>
								<input type="file" class="form-control" id="fileEdit" name="fileEdit" onchange="uploadEdit()">
							</div>
							<input type="hidden" class="form-control" id="avatar" name="avatar">
	        		 		<div class="form-group">
        		 				<label>Email</label>
        		 				<input type="email" class="form-control" id="email" name="email" placeholder="Your email" value="{{Auth::user()->email}}">
        		 			</div>
        		 			<div class="form-group">
        		 				<label>Mobile</label>
        		 				<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Your mobile" value="{{Auth::user()->mobile}}">
        		 			</div>
        		 			<div class="form-group">
        		 				<label>Address</label>
        		 				<input type="text" class="form-control" id="address" name="address" placeholder="Your address" value="{{Auth::user()->address}}">
        		 			</div>
        		 			<div class="form-group">
        		 				<label>Birthday</label>
        		 				<input type="date" class="form-control" id="birthday" name="birthday" value="{{Auth::user()->birthday}}">
        		 			</div>
		        		 </form> 
		        	</div>
		      	</div>
		    	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <button type="button" onclick="saveProfile()" class="btn btn-primary">Save changes</button>
		      	</div>
		    </div>
	  	</div>
	</div>
<!-- Modal -->
<div class="modal fade" id="addPost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><h2>Add New Post</h2></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body col-xs-12 col-sm-12 col-md-12">
                <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1" style="margin-top: 3%;">
                    <form action="" method="POST" class="form-horizontal" role="form">  
                            <div class="form-group">
                                <label>Type</label>
                                <div>
                                    <select name="addType" id="addType" class="form-control" required="required">
                                        @foreach($category_all as $category)
                                        @if($category->slug != "")
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>                          
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" id="addTitle" name="title" placeholder="title" required="true">
                                <p id="add_title_error" style="color: red"></p>
                            </div>
                            <div class="form-group">
                                <input type="hidden" class="form-control" id="author" name="author" value="{{Auth::user()->name}}">
                            </div>

                            <div class="form-group">
                                <label>Description</label>
                                <input type="text" class="form-control" id="addDescription" name="addDescription" placeholder="description" required="true">
                                <p id="add_description_error" style="color: red"></p>
                            </div>

                            <div class="form-group">
                                <label>Thumbnail</label>
                                <div id="img_before">

                                </div>
                                <input type="file" class="form-control" id="img" name="img" onchange="upload_thumbnail()">
                            </div>
                            <input type="hidden" class="form-control" id="addThumbnail" name="addThumbnail">

                            
                            <div class="form-group">
                                <label>Nội dung <span style="color: red">*</span></label>
                                <textarea style="border:1px" class="form-control" id="addContent" name="addContent" required="true" cols="60" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="hidden" class="form-control" id="addUserId" name="addContent" value="{{Auth::user()->id}}">
                            </div>
                            

                            {{csrf_field()}}

                            
                            <div class="form-group">
                                <label>Tag</label>
                                <input type="text" value="" id="taginput" data-role="tagsinput" />
                            </div>
                    </form>
                </div>
                <div class="col-xs-3 col-xs-offset-1 col-sm-3 col-sm-offset-1 col-md-3 col-md-offset-1" style="margin-top: 3%;">
                    
                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" onclick="SaveNewPost()" class="btn btn-primary">Save changes</button>
                </div>
            </div>
    </div>
</div>
@endsection