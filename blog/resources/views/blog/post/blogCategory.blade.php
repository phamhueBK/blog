@extends('blog/layouts/header_footer')
@section('content')
<div class="about">
	<div class="container">
        <div class="about-main">

        
            <div class="col-md-10 col-md-offset-1"> 
            	@if(count($category_children) < 2)
                    
	            	@for($i = 0; $i < count($posts); $i=$i+2)
	                    <div class="a-1">
	                        <div class="col-md-6 abt-left">
	                            <a href="{{url('post/detail?title='.$posts[$i]->slug)}}"><img height="250px" src="{{$posts[$i]->thumbnail}}" alt="" /></a>
	                            <a href="{{url('find_post_by_category?category='.$category_children[0]->slug)}}"><h6>{{$category_children[0]->name}}</h6></a>
	                            <h3><a href="{{url('post/detail?title='.$posts[$i]->slug)}}">{{$posts[$i]->title}}</a></h3>
	                            <p>{{$posts[$i]->description}}</p>
	                            <label>{{$posts[$i]->created_at}}</label>
	                        </div>
	                        @if($i+1 < count($posts))
	                        <div class="col-md-6 abt-left">
	                            <a href="{{url('post/detail?title='.$posts[$i+1]->slug)}}"><img height="250px" src="{{$posts[$i+1]->thumbnail}}" alt="" /></a>
	                            <a href="{{url('find_post_by_category?category='.$category_children[0]->slug)}}"><h6>{{$category_children[0]->name}}</h6></a>
	                            <h3><a href="{{url('post/detail?title='.$posts[$i+1]->slug)}}">{{$posts[$i+1]->title}}</a></h3>
	                            <p>{{$posts[$i+1]->description}}</p>
	                            <label>{{$posts[$i+1]->created_at}}</label>
	                        </div>
	                        @endif
	                        <div class="clearfix"></div>
	                    </div>
                    @endfor
                
                    {{$posts->links()}}
                    
                @else
                <div class="about-tre">
                    @foreach($category_children as $category)
                    @if(count($posts[$category->id]) > 0)
                    <div class="a-1">
                        <div class="col-md-6 abt-left">
                            <a href="{{url('post/detail?title='.$posts[$category->id][0]->slug)}}"><img height="250px" src="{{$posts[$category->id][0]->thumbnail}}" alt="" /></a>
                            <a href="{{url('find_post_by_category?category='.$category->slug)}}"><h6>{{$category->name}}</h6></a>
                            <h3><a href="{{url('post/detail?title='.$posts[$category->id][0]->slug)}}">{{$posts[$category->id][0]->title}}</a></h3>
                            <p>{{$posts[$category->id][0]->description}}</p>
                            <label>{{$posts[$category->id][0]->created_at}}</label>
                        </div>
                        
                        <div class="col-md-6 abt-left">
                            <a href="{{url('post/detail?title='.$posts[$category->id][1]->slug)}}"><img height="250px" src="{{$posts[$category->id][1]->thumbnail}}" alt="" /></a>
                            <a href="{{url('find_post_by_category?category='.$category->slug)}}"><h6>{{$category->name}}</h6></a>
                            <h3><a href="{{url('post/detail?title='.$posts[$category->id][1]->slug)}}">{{$posts[$category->id][1]->title}}</a></h3>
                            <p>{{$posts[$category->id][1]->description}}</p>
                            <label>{{$posts[$category->id][1]->created_at}}</label>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                    @endif
                    @endforeach
                    
                </div>  
                @endif
            </div>

            <div class="clearfix"></div>            
        </div>      
    </div>
</div>
@endsection
@section('slide')
    <!--slide-starts-->
    <div class="slide">
        <div class="container">
            <div class="fle-xsel">
            <ul id="flexiselDemo3">
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-1.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-2.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>           
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-3.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>       
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-5.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>   
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-5.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>   
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-6.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>               
            </ul>
                            
                             <script type="text/javascript">
                                $(window).load(function() {
                                    
                                    $("#flexiselDemo3").flexisel({
                                        visibleItems: 5,
                                        animationSpeed: 1000,
                                        autoPlay: true,
                                        autoPlaySpeed: 3000,            
                                        pauseOnHover: true,
                                        enableResponsiveBreakpoints: true,
                                        responsiveBreakpoints: { 
                                            portrait: { 
                                                changePoint:480,
                                                visibleItems: 2
                                            }, 
                                            landscape: { 
                                                changePoint:640,
                                                visibleItems: 3
                                            },
                                            tablet: { 
                                                changePoint:768,
                                                visibleItems: 3
                                            }
                                        }
                                    });
                                    
                                });
                                </script>
                                <script type="text/javascript" src="{{asset('js/blog/jquery.flexisel.js')}}"></script>
                    <div class="clearfix"> </div>
            </div>
        </div>
    </div>  
    <!--slide-end-->