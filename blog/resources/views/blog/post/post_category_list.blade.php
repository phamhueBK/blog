@extends('blog/layouts/header_footer')
@section('content')
<div class="about">
	<div class="container">
        <div class="about-main">

        
            <div class="col-md-10 col-md-offset-1">
                
                <div class="about-tre">
                    @for($i = 0; $i < count($data); $i=$i+2)
                    <div class="a-1">
                        <div class="col-md-6 abt-left">
                            <a href="{{url('post/detail?title='.$data[$i]->slug)}}"><img height="250px" src="{{$data[$i]->thumbnail}}" alt="" /></a>
                            <a href="{{url('find_post_by_category?category='.$category_post->slug)}}"><h6>{{$category_post->name}}</h6></a>
                            <h3><a href="{{url('post/detail?title='.$data[$i]->slug)}}">{{$data[$i]->title}}</a></h3>
                            <p>{{$data[$i]->description}}</p>
                            <label>{{$data[$i]->created_at}}</label>
                        </div>
                        @if($i+1 < count($data))
                        <div class="col-md-6 abt-left">
                            <a href="{{url('post/detail?title='.$data[$i+1]->slug)}}"><img height="250px" src="{{$data[$i+1]->thumbnail}}" alt="" /></a>
                            <a href="{{url('find_post_by_category?category='.$category_post->slug)}}"><h6>{{$category_post->name}}</h6></a>
                            <h3><a href="{{url('post/detail?title='.$data[$i+1]->slug)}}">{{$data[$i+1]->title}}</a></h3>
                            <p>{{$data[$i+1]->description}}</p>
                            <label>{{$data[$i+1]->created_at}}</label>
                        </div>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    @endfor
                
                    {{$data->links()}}
                    <!--
                    <div class="a-1">
                        <div class="col-md-6 abt-left">
                            <a href="single.html"><img src="{{asset('img/blog/c-5.jpg')}}" alt="" /></a>
                            <h6>Find The Most</h6>
                            <h3><a href="single.html">Tasty Coffee</a></h3>
                            <p>Vivamus interdum diam diam, non faucibus tortor consequat vitae. Proin sit amet augue sed massa pellentesque viverra. Suspendisse iaculis purus eget est pretium aliquam ut sed diam.</p>
                            <label>May 29, 2015</label>
                        </div>
                        <div class="col-md-6 abt-left">
                            <a href="single.html"><img src="{{asset('img/blog/c-6.jpg')}}" alt="" /></a>
                            <h6>Find The Most</h6>
                            <h3><a href="single.html">Tasty Coffee</a></h3>
                            <p>Vivamus interdum diam diam, non faucibus tortor consequat vitae. Proin sit amet augue sed massa pellentesque viverra. Suspendisse iaculis purus eget est pretium aliquam ut sed diam.</p>
                            <label>May 29, 2015</label>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="a-1">
                        <div class="col-md-6 abt-left">
                            <a href="single.html"><img src="{{asset('img/blog/c-7.jpg')}}" alt="" /></a>
                            <h6>Find The Most</h6>
                            <h3><a href="single.html">Tasty Coffee</a></h3>
                            <p>Vivamus interdum diam diam, non faucibus tortor consequat vitae. Proin sit amet augue sed massa pellentesque viverra. Suspendisse iaculis purus eget est pretium aliquam ut sed diam.</p>
                            <label>May 29, 2015</label>
                        </div>
                        <div class="col-md-6 abt-left">
                            <a href="single.html"><img src="{{asset('img/blog/c-8.jpg')}}" alt="" /></a>
                            <h6>Find The Most</h6>
                            <h3><a href="single.html">Tasty Coffee</a></h3>
                            <p>Vivamus interdum diam diam, non faucibus tortor consequat vitae. Proin sit amet augue sed massa pellentesque viverra. Suspendisse iaculis purus eget est pretium aliquam ut sed diam.</p>
                            <label>May 29, 2015</label>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    -->
                </div>  
            </div>


            
            <div class="clearfix"></div>            
        </div>      
    </div>
</div>
@endsection
@section('slide')
    <!--slide-starts-->
    <div class="slide">
        <div class="container">
            <div class="fle-xsel">
            <ul id="flexiselDemo3">
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-1.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-2.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>           
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-3.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>       
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-5.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>   
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-5.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>   
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-6.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>               
            </ul>
                            
                             <script type="text/javascript">
                                $(window).load(function() {
                                    
                                    $("#flexiselDemo3").flexisel({
                                        visibleItems: 5,
                                        animationSpeed: 1000,
                                        autoPlay: true,
                                        autoPlaySpeed: 3000,            
                                        pauseOnHover: true,
                                        enableResponsiveBreakpoints: true,
                                        responsiveBreakpoints: { 
                                            portrait: { 
                                                changePoint:480,
                                                visibleItems: 2
                                            }, 
                                            landscape: { 
                                                changePoint:640,
                                                visibleItems: 3
                                            },
                                            tablet: { 
                                                changePoint:768,
                                                visibleItems: 3
                                            }
                                        }
                                    });
                                    
                                });
                                </script>
                                <script type="text/javascript" src="{{asset('js/blog/jquery.flexisel.js')}}"></script>
                    <div class="clearfix"> </div>
            </div>
        </div>
    </div>  
    <!--slide-end-->
@endsection