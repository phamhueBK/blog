@extends('blog/layouts/header_footer')
@section('content')
	<div class="single col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md--10 col-md-offset-1">
		<center><h2>Kết quả cho chủ đề "{{$category}}"</h2></center>
		@foreach($data as $value)
		<div class="container">
			<div class="single-top">
					
				<div class=" single-grid" style="margin-bottom: 2%">
					<h4><a href="{{url('post/detail?title='.$value->slug)}}"> {{$value->title}}</a></h4>				
						<ul class="blog-ic">
							<li><a href="#"><span> <i  class="glyphicon glyphicon-user"> </i>Super user</span> </a> </li>
	  						 <li><span><i class="glyphicon glyphicon-time"> </i>{{$value->created_at}}</span></li>		  						 	
	  						 <li><span><i class="glyphicon glyphicon-eye-open"> </i>Views: {{$value->views}}</span></li>
	  					</ul>		  						
					<div>
						<?php echo $value->description; ?>
					</div>
				</div>
				
				
			</div>	
		</div>	
		@endforeach				
	</div>
@endsection