@extends('blog/layouts/header_footer')
@section('content')
	<div class="single col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md--10 col-md-offset-1">
		<div class="container">
				<div class="single-top">
						<a href="{{$data->thumbnail}}"><center><img width="90%" class="img-responsive" src="{{$data->thumbnail}}" alt=" "></center></a>
					<center>
					<div style="margin-top: 3%; margin-bottom: 3%">
						@foreach($category_post as $value)
						<a href="{{url('find_post_by_category?category='.$value->slug)}}" class="btn btn-primary">{{$value->name}}</a>
						@endforeach
					</div>
					</center>
					<div class=" single-grid">
						<h4>{{$data->title}}</h4>				
							<ul class="blog-ic">
								<li><a href="#"><span> <i  class="glyphicon glyphicon-user"> </i>Super user</span> </a> </li>
		  						 <li><span><i class="glyphicon glyphicon-time"> </i>{{$data->created_at}}</span></li>		  						 	
		  						 <li><span><i class="glyphicon glyphicon-eye-open"> </i>Views: {{$data->views}}</span></li>
		  					</ul>		  						
						<div>
							<?php echo $data->content; ?>
						</div>
					</div>
					<div class="comments heading">
						<h3>Comments</h3>
						<div class="media">
					      	<div class="media-body">
						        <h4 class="media-heading">	Richard Spark</h4>
						        <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs .  </p>
					      	</div>
					      <div class="media-right">
					        <a href="#">
								<img src="{{asset('img/blog/si.png')}}" alt=""> </a>
					      </div>
					 </div>
					  <div class="media">
					      <div class="media-left">
					        <a href="#">
					        	<img src="{{asset('img/blog/si.png')}}" alt="">
					        </a>
					      </div>
					      <div class="media-body">
					        <h4 class="media-heading">Joseph Goh</h4>
					        <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs .  </p>
					      </div>
					    </div>
    				</div>
    				<div class="comment-bottom heading">
    					<h3>Leave a Comment</h3>
    					<form>	
						<input type="text" value="Name" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Name';}">
						<input type="text" value="Email" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Email';}">
						<input type="text" value="Subject" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Subject';}">
						<textarea cols="77" rows="6" value=" " onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
							<input type="submit" value="Send">
					</form>
    				</div>
				</div>	
			</div>					
	</div>
@endsection