@extends('blog/layouts/header_footer')
@section('content')
<div class="about">
	<div class="container">
        <div class="about-main">

        
            <div class="col-md-8 about-left">
                
                <div class="about-tre">
                    @for($i = 0; $i < count($data); $i=$i+2)
                    <div class="a-1">
                        <div class="col-md-6 abt-left">
                            <a href="{{url('post/detail?title='.$data[$i]->slug)}}"><img height="250px" src="{{$data[$i]->thumbnail}}" alt="" /></a>
                            <a href="{{url('find_post_by_category?category='.$category_post[$data[$i]->id]->slug)}}"><h6>{{$category_post[$data[$i]->id]->name}}</h6></a>
                            <h3><a href="{{url('post/detail?title='.$data[$i]->slug)}}">{{$data[$i]->title}}</a></h3>
                            <p>{{$data[$i]->description}}</p>
                            <label>{{$data[$i]->created_at}}</label>
                        </div>
                        @if($i+1 < count($data))
                        <div class="col-md-6 abt-left">
                            <a href="{{url('post/detail?title='.$data[$i+1]->slug)}}"><img height="250px" src="{{$data[$i+1]->thumbnail}}" alt="" /></a>
                            <a href="{{url('find_post_by_category?category='.$category_post[$data[$i+1]->id]->slug)}}"><h6>{{$category_post[$data[$i+1]->id]->name}}</h6></a>
                            <h3><a href="{{url('post/detail?title='.$data[$i+1]->slug)}}">{{$data[$i+1]->title}}</a></h3>
                            <p>{{$data[$i+1]->description}}</p>
                            <label>{{$data[$i+1]->created_at}}</label>
                        </div>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    @endfor
                    {{$data->links()}}
                    <!--
                    <div class="a-1">
                        <div class="col-md-6 abt-left">
                            <a href="single.html"><img src="{{asset('img/blog/c-5.jpg')}}" alt="" /></a>
                            <h6>Find The Most</h6>
                            <h3><a href="single.html">Tasty Coffee</a></h3>
                            <p>Vivamus interdum diam diam, non faucibus tortor consequat vitae. Proin sit amet augue sed massa pellentesque viverra. Suspendisse iaculis purus eget est pretium aliquam ut sed diam.</p>
                            <label>May 29, 2015</label>
                        </div>
                        <div class="col-md-6 abt-left">
                            <a href="single.html"><img src="{{asset('img/blog/c-6.jpg')}}" alt="" /></a>
                            <h6>Find The Most</h6>
                            <h3><a href="single.html">Tasty Coffee</a></h3>
                            <p>Vivamus interdum diam diam, non faucibus tortor consequat vitae. Proin sit amet augue sed massa pellentesque viverra. Suspendisse iaculis purus eget est pretium aliquam ut sed diam.</p>
                            <label>May 29, 2015</label>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="a-1">
                        <div class="col-md-6 abt-left">
                            <a href="single.html"><img src="{{asset('img/blog/c-7.jpg')}}" alt="" /></a>
                            <h6>Find The Most</h6>
                            <h3><a href="single.html">Tasty Coffee</a></h3>
                            <p>Vivamus interdum diam diam, non faucibus tortor consequat vitae. Proin sit amet augue sed massa pellentesque viverra. Suspendisse iaculis purus eget est pretium aliquam ut sed diam.</p>
                            <label>May 29, 2015</label>
                        </div>
                        <div class="col-md-6 abt-left">
                            <a href="single.html"><img src="{{asset('img/blog/c-8.jpg')}}" alt="" /></a>
                            <h6>Find The Most</h6>
                            <h3><a href="single.html">Tasty Coffee</a></h3>
                            <p>Vivamus interdum diam diam, non faucibus tortor consequat vitae. Proin sit amet augue sed massa pellentesque viverra. Suspendisse iaculis purus eget est pretium aliquam ut sed diam.</p>
                            <label>May 29, 2015</label>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    -->
                </div>  
            </div>


            <div class="col-md-4 about-right heading">
                <div class="abt-1">
                    <h3>ABOUT US</h3>
                    <div class="abt-one">
                        <img src="{{asset('img/blog/c-2.jpg')}}" alt="" />
                        <p>Quisque non tellus vitae mauris luctus aliquam sit amet id velit. Mauris ut dapibus nulla, a dictum neque.</p>
                        <div class="a-btn">
                            <a href="single.html">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="abt-2">
                    <h3>YOU MIGHT ALSO LIKE</h3>
                        <div class="might-grid">
                            <div class="grid-might">
                                <a href="single.html"><img src="{{asset('img/blog/c-12.jpg')}}" class="img-responsive" alt=""> </a>
                            </div>
                            <div class="might-top">
                                <h4><a href="single.html">Duis consectetur gravida</a></h4>
                                <p>Nullam non magna lobortis, faucibus erat eu, consequat justo. Suspendisse commodo nibh odio.</p> 
                            </div>
                            <div class="clearfix"></div>
                        </div>  
                        <div class="might-grid">
                            <div class="grid-might">
                                <a href="single.html"><img src="{{asset('img/blog/c-10.jpg')}}" class="img-responsive" alt=""> </a>
                            </div>
                            <div class="might-top">
                                <h4><a href="single.html">Duis consectetur gravida</a></h4>
                                <p> Nullam non magna lobortis, faucibus erat eu, consequat justo. Suspendisse commodo nibh odio.</p> 
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="might-grid">
                            <div class="grid-might">
                                <a href="single.html"><img src="{{asset('img/blog/c-11.jpg')}}" class="img-responsive" alt=""> </a>
                            </div>
                            <div class="might-top">
                                <h4><a href="single.html">Duis consectetur gravida</a></h4>
                                <p> Nullam non magna lobortis, faucibus erat eu, consequat justo. Suspendisse commodo nibh odio.</p> 
                            </div>
                            <div class="clearfix"></div>
                        </div>                          
                </div>
                <div class="abt-2">
                    <h3>ARCHIVES</h3>
                    <ul>
                        <li><a href="single.html">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </a></li>
                        <li><a href="single.html">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</a></li>
                        <li><a href="single.html">When an unknown printer took a galley of type and scrambled it to make a type specimen book. </a> </li>
                        <li><a href="single.html">It has survived not only five centuries, but also the leap into electronic typesetting</a> </li>
                        <li><a href="single.html">Remaining essentially unchanged. It was popularised in the 1960s with the release of </a> </li>
                        <li><a href="single.html">Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing </a> </li>
                        <li><a href="single.html">Software like Aldus PageMaker including versionsof Lorem Ipsum.</a> </li>
                    </ul>   
                </div>
                <div class="abt-2">
                    <h3>NEWS LETTER</h3>
                    <div class="news">
                        <form>
                            <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" />
                            <input type="submit" value="Subscribe">
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>            
        </div>      
    </div>
</div>
@endsection
@section('slide')
    <!--slide-starts-->
    <div class="slide">
        <div class="container">
            <div class="fle-xsel">
            <ul id="flexiselDemo3">
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-1.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-2.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>           
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-3.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>       
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-5.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>   
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-5.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>   
                <li>
                    <a href="#">
                        <div class="banner-1">
                            <img src="{{asset('img/blog/s-6.jpg')}}" class="img-responsive" alt="">
                        </div>
                    </a>
                </li>               
            </ul>
                            
                             <script type="text/javascript">
                                $(window).load(function() {
                                    
                                    $("#flexiselDemo3").flexisel({
                                        visibleItems: 5,
                                        animationSpeed: 1000,
                                        autoPlay: true,
                                        autoPlaySpeed: 3000,            
                                        pauseOnHover: true,
                                        enableResponsiveBreakpoints: true,
                                        responsiveBreakpoints: { 
                                            portrait: { 
                                                changePoint:480,
                                                visibleItems: 2
                                            }, 
                                            landscape: { 
                                                changePoint:640,
                                                visibleItems: 3
                                            },
                                            tablet: { 
                                                changePoint:768,
                                                visibleItems: 3
                                            }
                                        }
                                    });
                                    
                                });
                                </script>
                                <script type="text/javascript" src="{{asset('js/blog/jquery.flexisel.js')}}"></script>
                    <div class="clearfix"> </div>
            </div>
        </div>
    </div>  
    <!--slide-end-->
@endsection