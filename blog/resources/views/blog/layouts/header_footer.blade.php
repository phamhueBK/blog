<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Blog</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Coffee Break Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="{{asset('css/blog/bootstrap.css')}}" rel='stylesheet' type='text/css' />
<link href="{{asset('css/blog/style.css')}}" rel='stylesheet' type='text/css' />
<script src="{{asset('js/blog/jquery.min.js')}}"></script>
<!---- start-smoth-scrolling---->
<script type="text/javascript" src="{{asset('js/blog/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('js/blog/easing.js')}}"></script>
<script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>

<!--start-smoth-scrolling-->
<!-- toastr notifications -->
    <script type="text/javascript" src="{{asset('js/blog/toastr.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css
">

@yield('script_profile')
</head>
<body>
    <!--header-top-starts-->
    <div class="header-top">
        <div class="container">
            <div class="head-main">
                <a href="index.html"><img src="{{asset('img/blog/logo-1.png')}}" alt="" /></a>
            </div>
        </div>
    </div>
    <!--header-top-end-->
    <!--start-header-->
    <div class="header">
        <div class="container">
            <div class="head">
            <div class="navigation">
                 <span class="menu"></span> 
                    <ul class="navig">
                        @foreach($categories as $category)
                            <li><a href="{{url('/blog?go='.$category->slug)}}">{{$category->name}}</a></li>
                        @endforeach
                        @guest
                            <li><a href="{{url('login')}}">Login</a></li>
                            <li><a href="{{url('register')}}">Register</a></li>
                            @else
                            <li><a href="{{url('user/profile')}}">Hello, {{Auth::user()->name}}</a></li>
                            <li>
                                <a href=""
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-fw"></i> 
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                            @endguest
                        <!--<li><a href="about.html">About</a></li>
                        <li><a href="gallery.html">Gallery</a></li>
                        <li><a href="typo.html">Typo</a></li>
                        <li><a href="contact.html">Contact</a></li>-->
                    </ul>

            </div>
            @if(isset($category_children))
            <div class="navigation">
                 <span class="menu"></span> 
                    <ul class="navig">
                        @foreach($category_children as $category)
                            <li><a href="{{url('blog/post?category='.$category->slug)}}" style="color: gray">{{$category->name}}</a></li>
                        @endforeach
                        <!--<li><a href="about.html">About</a></li>
                        <li><a href="gallery.html">Gallery</a></li>
                        <li><a href="typo.html">Typo</a></li>
                        <li><a href="contact.html">Contact</a></li>-->
                    </ul>
                    
            </div>
            @endif
            <!--<div class="header-right">
                <div class="search-bar">
                    <input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}">
                    <input type="submit" value="">
                </div>
                <ul>
                    <li><a href="#"><span class="fb"> </span></a></li>
                    <li><a href="#"><span class="twit"> </span></a></li>
                    <li><a href="#"><span class="pin"> </span></a></li>
                    <li><a href="#"><span class="rss"> </span></a></li>
                    <li><a href="#"><span class="drbl"> </span></a></li>
                </ul>
            </div>-->
                <div class="clearfix"></div>
            </div>
            </div>
        </div>  
    <!-- script-for-menu -->
    <!-- script-for-menu -->
        <script>
            $("span.menu").click(function(){
                $(" ul.navig").slideToggle("slow" , function(){
                });
            });
        </script>
    <!-- script-for-menu -->
    
    <!--about-starts-->
    
        @yield('content')
    
    <!--about-end-->
        @yield('slide')
    <!--footer-starts-->
    <script src="{{asset('js/app.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
        $('#table').DataTable();
    } );
    </script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
    <div class="footer">
        <div class="container">
            <div class="footer-text">
                <p>© 2015 Coffee Break. All Rights Reserved | Design by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
            </div>
        </div>
    </div>
    <!--footer-end-->
</body>
</html>