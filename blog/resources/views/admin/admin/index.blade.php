@extends('admin/layouts/header_footer')
@section('script_post')
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-tagsinput.css')}}">
<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}">
<script src="{{asset('js/jquery-2.2.4.js')}}" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

<script type="text/javascript" src="{{asset('js/admin/listAdminIndex.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap-tagsinput.js')}}"></script>
<script type="text/javascript" src="{{asset('js/sweetalert.js')}}"></script>
@endsection
@section('content')
	 <section id="main-content">
        <section class="wrapper site-min-height">
          	<div class="col-xs-12 col-sm-12 col-md-12" style="margin-top: 5%; margin-bottom: 5%">
				<table class="table table-hover" id="table">
					<thead>
						<tr>
							<th colspan="7"><a onclick="btnCreateUser()" class="btn btn-success">Thêm mới</a></th>
						</tr>
						<tr>
							<th>Name</th>
							<th>Avatar</th>
							<th>Email</th>
							<th>Address</th>
							<th>Mobile</th>
							<th>Status</th>
							<th>
								Action
							</th>
						</tr>
					</thead>
					<tbody id="menu">
						@foreach($data as $value)
						<tr id="user_{{$value['id']}}">
							
							<td>{{$value['name']}}</td>
							<td><img src="{{$value['avatar']}}" width="100px"/></td>
							<td>{{$value['email']}}</td>
							<td>{{$value['address']}}</td>
							<td>{{$value['mobile']}}</td>
							<td>
								@if($value['status'] == "Đang hoạt động")
								Active
								@else
								Not Active
								@endif
							</td>
							<td>
								<a onclick="btnShow('{{$value['id']}}')" class="btn btn-success"><i class="fa fa-plus-circle" id="show_{{$value['id']}}" aria-hidden="true"></i></a>
								<a onclick="btnEdit('{{$value['id']}}')" class="btn btn-success"><span class="glyphicon glyphicon-edit" id="show_{{$value['id']}}"></span></a>
								@if($value['status'] == "Đang hoạt động")
								<a onclick="btnDelete('{{$value['id']}}')" class="btn btn-success"><i id="delete_{{$value['id']}}" class="glyphicon glyphicon-remove" aria-hidden="true"></i></a>
								<a onclick="btnApprove('{{$value['id']}}')" class="btn btn-success" style="display: none"><i id="repeat_{{$value['id']}}" class="fa fa-repeat" aria-hidden="true"></i></a>
								@else
								<a onclick="btnDelete('{{$value['id']}}')" class="btn btn-success" style="display: none"><i id="delete_{{$value['id']}}" class="glyphicon glyphicon-remove" aria-hidden="true"></i></a>
								<a onclick="btnApprove('{{$value['id']}}')" class="btn btn-success"><i id="repeat_{{$value['id']}}" class="fa fa-repeat" aria-hidden="true"></i></a>
								@endif
							</td>
							
						</tr>
						<tr style="display: none" id="info_{{$value['id']}}">

							<td colspan="5">
								<input type="hidden" id="danhDau" value="hide">
								<b>Posts:</b> {{$value['address']}}<br>
								<b>Mobile:</b> {{$value['mobile']}}<br>
								<b>Birthday:</b> {{$value['birthday']}}<br>
								<b>Created_at:</b> {{$value['created_at']}}<br>
								<b>Updated_at:</b> {{$value['updated_at']}}<br>
								<b>Status:</b> <span id="status_{{$value['id']}}">{{$value['status']}}</span><br>
								<b>Avatar:</b> <img src="{{$value['avatar']}}" width="500px"/><br>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			
		</section>
    </section>
<!-- Modal -->
<div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Update Admin's Infomation</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	     	</div>
	      	<div class="modal-body">
	        	<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
	        		<form action="" method="POST" class="form-horizontal" role="form">
	        			<div class="form-group">
							<input type="hidden" class="form-control" id="editId" name="editId">
						</div>
							
						<div class="form-group">
							<label >Name</label>
							<input type="text" class="form-control" id="editName" name="editName" placeholder="Name" required="true">
						</div>
						<div class="form-group">
							<label >Email</label>
							<input type="email" class="form-control" id="editEmail" name="editEmail" placeholder="Email" required="true">
						</div>
						<div class="form-group">
							<label >Password</label>
							<input type="password" class="form-control" id="editPassword" name="editPassword" placeholder="Password" required="true">
						</div>
						{{csrf_field()}}
	        		</form>
	        	</div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		        <button type="button" onclick="btnSaveEditUser()" class="btn btn-primary">Save changes</button>
	      	</div>
	    </div>
  	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="createUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  	<div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Insert New Admin</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
	     	</div>
	      	<div class="modal-body">
	        	<div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1">
	        		<form action="" method="POST" class="form-horizontal" role="form">
	        			<div class="form-group">
							<label >Name</label>
							<input type="text" class="form-control" id="addName" name="addName" placeholder="Name" required="true">
						</div>
						<div class="form-group">
							<label >Email</label>
							<input type="email" class="form-control" id="addEmail" name="addEmail" placeholder="Email" required="true">
						</div>
						<div class="form-group">
							<label >Password</label>
							<input type="password" class="form-control" id="addPassword" name="addPassword" placeholder="Password" required="true">
						</div>
						{{csrf_field()}}
	        		</form>
	        	</div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
		        <button type="button" onclick="btnSaveCreateUser()" class="btn btn-primary">Save changes</button>
	      	</div>
	    </div>
  	</div>
</div>
@endsection