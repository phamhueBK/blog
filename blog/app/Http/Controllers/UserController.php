<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Category;
use App\Post;
use DB;

class UserController extends Controller
{
    public function index(){
        $posts = DB::table('posts')
    			->where('user_id', '=', Auth::user()->id)
    			->orderBy('updated_at', 'desc')
    			->paginate(10);
    	$posts_for_find = DB::table('posts')
    			->where('user_id', '=', Auth::user()->id)
    			->orderBy('updated_at', 'desc')
    			->get();
        $categories = array();
        

    	foreach ($posts_for_find as $key => $value) {
    		$category_find = DB::table('categories')
                    ->join('post_categories', 'post_categories.category_id', '=', 'categories.id')
                    ->where('post_categories.post_id', '=', $value->id)
                    ->get();
            $categories[$value->id] = $category_find;

            
    	}
    	$category_children = DB::table('categories')
    			->where('parent_id', '>', 0)
    			->orWhere('slug', '=', 'tam-su.html')
    			->get();
        $category = DB::table('categories')
    			->where('parent_id', '=', 0)
    			->get();
        $category_all = Category::get();
        return view('blog/user/profile', ['post' => $posts, 'categories' => $category, 'myPost' => $posts, 'myCategory' => $categories, 'category_child' => $category_children, 'category_all' => $category_all]);
    }
    public function upload_img(Request $request){
        $duoi = explode('.', $_FILES['file']['name']); // tách chuỗi khi gặp dấu .
        $duoi = $duoi[(count($duoi)-1)];//lấy ra đuôi file
        //Kiểm tra xem có phải file ảnh không
        if($duoi === 'jpg' || $duoi === 'png' || $duoi === 'gif' || $duoi === 'jpeg'){
            //tiến hành upload
            $url = 'img/' . $_FILES['file']['name'];
            if(move_uploaded_file($_FILES['file']['tmp_name'], $url)){
                //Nếu thành công
                echo $url;
            } else{ //nếu k thành công
                echo "Có lỗi"; //in ra thông báo
            }
        } else{ //nếu k phải file ảnh
            echo "File không phải ảnh"; //in ra thông báo
        }
    }
    
    public function update(Request $request){
        $user = User::find(Auth::user()->id);
        
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->address = $request->address;
        $user->birthday = $request->birthday;
        if($request->avatar != "")
            $user->avatar = $request->avatar;

        $user->save();

        Auth::user()->name = $user->name;
        Auth::user()->email = $user->email;
        Auth::user()->mobile = $user->mobile;
        Auth::user()->address = $user->address;
        Auth::user()->birthday = $user->birthday;
        Auth::user()->avatar = $user->avatar;


        return $user;
    }

    public function blog_list(){
        $posts = DB::table('posts')->where('user_id', '=', Auth::user()->id)->orderBy('updated_at', 'desc')->get();
        foreach ($posts as $key => $value) {
            if($value->status == 0)
                $posts[$key]->status = "Chờ duyệt";
            elseif($value->status == 1)
                $posts[$key]->status = "Đã duyệt";
            else
                $posts[$key]->status = "Đã xóa";

        }
        
        $categories = array();
        $rejects = array();

    	foreach ($posts as $key => $value) {
    		$category_find = DB::table('categories')
                    ->join('post_categories', 'post_categories.category_id', '=', 'categories.id')
                    ->where('post_categories.post_id', '=', $value->id)
                    ->get();
            $categories[$value->id] = $category_find;

            $reject = DB::table('rejects')
                        ->where('post_id', '=', $value->id)
                        ->get();

            if(count($reject) > 0)
            {
                $rejects[$value->id] = $reject[0]->reason;
            }
    	}
    	$category_children = DB::table('categories')
    			->where('parent_id', '>', 0)
    			->orWhere('slug', '=', 'tam-su.html')
    			->get();
        $category = DB::table('categories')
    			->where('parent_id', '=', 0)
    			->get();
        return view('blog/user/blog_list', ['categories' => $category, 'myPost' => $posts, 'myCategory' => $categories, 'category_child' => $category_children, 'notes' => $rejects]);
    }
}
