<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {
        $check = Auth::guard('')->attempt(['email' => $request->email, 'password' => $request->password]);
        if ($check) {
            if(Auth::user()->status == 0)
                dd('Tài khoản đã bị vô hiệu hóa');
            else
                return redirect()->intended('/');
        } else {
            dd('Tai khoan khong dung');
        }
    }
    public function logout(Request $request)
    {
        Auth::guard('')->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
}
