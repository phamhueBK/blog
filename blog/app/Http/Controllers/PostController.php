<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use DB;
use Session;
use App\PostCategory;
use App\Category;
use App\Tag;
use App\PostTag;
use Auth;

class PostController extends Controller
{
	

    public function home(){
    	$post = DB::table('posts')
    			->where('status', '=', 1)
    			->orderBy('updated_at', 'desc')
    			->paginate(10);
    	$categories = array();
    	foreach ($post as $key => $value) {
    		$category_find = DB::table('categories')
                    ->join('post_categories', 'post_categories.category_id', '=', 'categories.id')
                    ->where('post_categories.post_id', '=', $value->id)
                    ->get();
            $categories[$value->id] = $category_find[0];
    	}
    	$category = DB::table('categories')
    			->where('parent_id', '=', 0)
    			->get();
    	/*echo "<pre>";
    	print_r($post);
    	echo "</pre>";
    	dd();*/
    	return view('blog/post/home', ['data' => $post, 'categories' => $category, 'category_post' => $categories]);
    }

    public function detail(){
    	$slug = $_GET['title'];
    	$post = DB::table('posts')
    			->where('slug', '=', $slug)
    			->get();
    	
    	$category_find = DB::table('categories')
                    ->join('post_categories', 'post_categories.category_id', '=', 'categories.id')
                    ->where('post_categories.post_id', '=', $post[0]->id)
                    ->get();
        $category = DB::table('categories')
    			->where('parent_id', '=', 0)
    			->get();
    	return view('blog/post/detail', ['data' => $post[0], 'category_post' => $category_find, 'categories' => $category]);
    }

    public function findByTag(){
    	$slug = $_GET['category'];
    	$category = DB::table('categories')
    			->where('slug', '=', $slug)
    			->get();
    	//dd($category);
    	$posts = DB::table('posts')
                    ->join('post_categories', 'post_categories.post_id', '=', 'posts.id')
                    ->where('post_categories.category_id', '=', $category[0]->id)
                    ->get();
        $category2 = DB::table('categories')
    			->where('parent_id', '=', 0)
    			->get();
    	return view('blog/post/rs_find_by_tag', ['data' => $posts, 'category' => $category[0]->name, 'categories' => $category2]);
    }

    public function redirect(){
    	$slug = $_GET['go'];
    	if($slug == 'truyen-ngan.html' || $slug == 'truyen-blog.html'
    	|| $slug == 'sach-hay.html' || $slug == 'tam-su.html'
    	|| $slug == 'tieu-thuyet.html'){
    		Session::put('category', $slug);
    		return redirect('/'.$slug)->with('category', $slug);
    	}
    	else
    		return redirect('/');
    }

    public function BlogCategory(){
    	$category = DB::table('categories')
    			->where('parent_id', '=', 0)
    			->get();
    	$slug = Session::get('category');

    	$category_parent = DB::table('categories')
    			->where('slug', '=', $slug)
    			->get();
    	//dd($slug);

    	$post = DB::table('posts')
                    ->join('post_categories', 'post_categories.post_id', '=', 'posts.id')
                    ->where('post_categories.category_id', '=', $category_parent[0]->id)
                    ->orderBy('posts.updated_at', 'desc')
	    			->paginate(10);
	    if(count($post) <= 0){
	    	$category_children = DB::table('categories')
    			->where('parent_id', '=', $category_parent[0]->id)
    			->get();

	    	$posts = array();

	    	foreach ($category_children as $key => $value) {
	    		$post = DB::table('posts')
	                    ->join('post_categories', 'post_categories.post_id', '=', 'posts.id')
	                    ->where('post_categories.category_id', '=', $value->id)
                        ->where('posts.status', '=', 1)
	                    ->orderBy('posts.updated_at', 'desc')
	                    ->offset(0)
	                	->limit(2)
	                    ->get();
                $posts[$value->id] = $post;
	    	}
	    }
	    else
	    {
	    	$category_children = $category_parent;
	    	$posts = $post;
	    }
    	
    	//dd($posts);
    	return view('blog/post/blogCategory', ['categories' => $category, 'category_children' => $category_children, 'posts' => $posts]);
    }

    public function showToCategory(){
    	if(isset($_GET['category'])){
    		$category = DB::table('categories')
    			->where('slug', '=', $_GET['category'])
    			->get();
	    	$post = DB::table('posts')
	                    ->join('post_categories', 'post_categories.post_id', '=', 'posts.id')
	                    ->where('post_categories.category_id', '=', $category[0]->id)
	                    ->orderBy('posts.updated_at', 'desc')
	    				->paginate(10);

	    	
	    	$categories = DB::table('categories')
	    			->where('parent_id', '=', 0)
	    			->get();
	    	/*echo "<pre>";
	    	print_r($post);
	    	echo "</pre>";
	    	dd();*/
	    	return view('blog/post/post_category_list', ['data' => $post, 'categories' => $categories, 'category_post' => $category[0]]);
    	}
    	else
    		return redirect('/');
    	
    }
    public function create(Request $req){
    	$data['title'] = $req->title;
    	$data['description'] = $req->description;
    	$data['content'] = $req->content;
    	
    	$data['status'] = 0;
    	$data['views'] = $req->views;
        $data['user_id'] = $req->user_id;
    	$data['admin_id'] = 0;
    	$data['slug'] = $data['title'];
    	if($req->thumbnail != "")
        	$data['thumbnail'] = $req->thumbnail;
        $data = Post::create($data);
        //Lay tag
        $tagArr = $req->tag;
        $tags = array();
        $vt = strpos($tagArr, ',');
        for($i = 0; $i < strlen($tagArr); $i++){
            $length = strlen($tagArr);
            $str = substr($tagArr, 0, $vt);
            if($vt > 0)
                $tagArr = substr($tagArr, $vt+1, $length - strlen($str));
            $tags[] = $str;
            $vt = strpos($tagArr, ',');
        }
        $tags[] = $tagArr;

        //Them moi tag
        foreach ($tags as $tag) {
            if($tag != ""){
                $tagFind = DB::table('tags')->where('slug', '=', $tag)->get();
                //Nếu tồn tại tag này rồi thì thêm mới post_tags
                if(count($tagFind) > 0){
                    $tag_id = $tagFind[0]->id;
                    $post_id = $data->id;
                    $post_tag['tag_id'] = $tag_id;
                    $post_tag['post_id'] = $post_id;
                    PostTag::create($post_tag);
                }
                //Nếu chưa có thì thêm mới trong tags và cả post_tags
                else
                {
                    //Thêm tag
                    $tagAdd['name'] = $tag;
                    $tagAdd['slug'] = $tag;
                    Tag::create($tagAdd);

                    //Thêm post_tags
                    $tag_find_after_create = DB::table('tags')->where('slug', '=', $tag)->get();
                    $post_tag['tag_id'] = $tag_find_after_create[0]->id;
                    $post_tag['post_id'] = $data->id;
                    PostTag::create($post_tag);
                }
            }
            
        }
        $category_id = $req->category_id;
        //Thêm category
        $post_category['post_id'] = $data->id;
        $post_category['category_id'] = $category_id;
        PostCategory::create($post_category);

        $category = Category::find($category_id);
        $data->category = $category;
        $data->author = Auth::user()->name;

        return $data;
    }
}
