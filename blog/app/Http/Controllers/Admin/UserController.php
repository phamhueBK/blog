<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function getList(){
        
        $data = User::get();
        foreach ($data as $key => $value) {
            if($value['status'] == 0)
                $data[$key]['status'] = "Không hoạt động";
            else
                $data[$key]['status'] = "Đang hoạt động";
        }
        return view('admin/user/list', ['data' => $data]);
    }

    public function findUser(){
    	$id = $_GET['id'];
    	$user = User::find($id);
    	return $user;
    }

    public function update(Request $request){

    	$user = User::find($request->id);

    	$user->email = $request->email;
    	$user->name = $request->name;
    	$user->password = bcrypt($request->password);
    	
    	$user->save();
    	
    	return $request;
    }

    public function create(Request $request){
    	$user['name'] = $request->name;
    	$user['email'] = $request->email;
    	$user['password'] = $request->password;
    	$user['status'] = 1;

    	$user = User::create($user);

    	return $user;
    }

    public function nonActive(Request $request){
    	$user = User::find($request->id);
    	$user->status = $request->status;
    	$user->save();

    	return $user;
    }

}
