<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Post;
use App\PostCategory;
use DB;

class CategoryController extends Controller
{
    public function index(){
		$data = Category::get();
		$postAll = Post::get();
		$post = array();

		$category_parents = DB::table('categories')->where('parent_id', '=', 0)->get();

		foreach ($data as $key => $value) {
			
			if($value->parent_id != 0){
				$parent = Category::find($value->parent_id);
				$data[$key]['parent'] = $parent;
			}
			$post[$value['id']] = $value->posts;
			
		}

		return view('admin/category/index', ['data' => $data, 'post' => $post, 'category_parents' => $category_parents]);
	}

	public function findCategory(){
		$slug = $_GET['title'];
        $data = DB::table('categories')->where('slug', '=', $slug)->get();
        $category = Category::find($data[0]->id);
        return $category;
	}

	public function update(Request $request){
		$category = Category::find($request->id);
		$category->name = $request->name;
		$category->parent_id = $request->parent_id;
        $category->save();


        $value = $category;

        if($value['parent_id'] != 0){
			$parent = Category::find($value['parent_id']);
			$category['parent'] = $parent;
		}

        return $category;
	}

	public function delete(Request $request){
		$data = Category::find($request->id);
		$postCategory = DB::table('post_categories')->where('category_id', '=', $request->id)->get();
		foreach ($postCategory as $key => $value) {
			$post_category = PostCategory::find($value->id);
			$post_category->delete();
		}
        $id = $request->id;
        $data->delete();

        return $id;
	}

	public function create(Request $request){
		$data['name'] = $request->name;
		$data['slug'] = $request->slug;

		$data['parent_id'] = $request->parent_id;
		$data['left'] = 0;
		$data['right'] = 0;
		$data = Category::create($data);
		if($data['parent_id'] != 0){
			$parent = Category::find($data['parent_id']);
			$data['parent'] = $parent;
		}
		return $data;
	}
}
