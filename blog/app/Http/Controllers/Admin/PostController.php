<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use App\Tag;
use App\PostTag;
use App\Category;
use App\PostCategory;
use App\Admin;
use DB;
use Auth;
use App\Reject;

class PostController extends Controller
{
    public function index(){
    	$data = DB::table('posts')->where('status', '=', 1)->get();
    	$categories = array();
        foreach ($data as $key => $value) {
            if($value->user_id != 0){
				$user = User::find($value->user_id);
				$data[$key]->author = $user['name'];
            }elseif($value->admin_id != 0){
            	$admin = Admin::find($value->admin_id);
            	$data[$key]->author = $admin['name'];
            }
            else
            	$data[$key]->author = 'master admin';

            $post = Post::find($value->id);
            $categories[$value->id] = $post->categories;
        }

        $category_all = Category::get();

    	return view('admin/post/index', ['data' => $data, 'controllTitle' => 'Post List', 'categories' => $categories, 'category_all' => $category_all]);
    }

    public function index_approve(){
        $data = DB::table('posts')->where('status', '=', 0)->get();
    	$categories = array();
        foreach ($data as $key => $value) {
            if($value->user_id != 0){
				$user = User::find($value->user_id);
				$data[$key]->author = $user['name'];
            }elseif($value->admin_id != 0){
            	$admin = Admin::find($value->admin_id);
            	$data[$key]->author = $admin['name'];
            }
            else
            	$data[$key]->author = 'master admin';

            $post = Post::find($value->id);
            $categories[$value->id] = $post->categories;
        }

        $category_all = Category::get();

    	return view('admin/post/index', ['data' => $data, 'controllTitle' => 'Approve Post', 'categories' => $categories, 'category_all' => $category_all]);
    }

    public function create(Request $req){

    	$data['title'] = $req->title;
    	$data['description'] = $req->description;
    	$data['content'] = $req->content;
    	
    	$data['status'] = 1;
    	$data['views'] = $req->views;
    	$data['admin_id'] = Auth::guard('admin')->user()->id;
    	$data['user_id'] = 0;
    	$data['slug'] = $data['title'];
        $data['thumbnail'] = $req->thumbnail;

        $category_id = $req->category_id;
        $data = Post::create($data);

        //$data->sluggable;
        //Lay tag
        $tagArr = $req->tag;
        $tags = array();
        $vt = strpos($tagArr, ',');
        for($i = 0; $i < strlen($tagArr); $i++){
            $length = strlen($tagArr);
            $str = substr($tagArr, 0, $vt);
            if($vt > 0)
                $tagArr = substr($tagArr, $vt+1, $length - strlen($str));
            $tags[] = $str;
            $vt = strpos($tagArr, ',');
        }
        $tags[] = $tagArr;

        //Them moi tag
        foreach ($tags as $tag) {
            if($tag != ""){
                $tagFind = DB::table('tags')->where('slug', '=', $tag)->get();
                //Nếu tồn tại tag này rồi thì thêm mới post_tags
                if(count($tagFind) > 0){
                    $tag_id = $tagFind[0]->id;
                    $post_id = $data->id;
                    $post_tag['tag_id'] = $tag_id;
                    $post_tag['post_id'] = $post_id;
                    PostTag::create($post_tag);
                }
                //Nếu chưa có thì thêm mới trong tags và cả post_tags
                else
                {
                    //Thêm tag
                    $tagAdd['name'] = $tag;
                    $tagAdd['slug'] = $tag;
                    Tag::create($tagAdd);

                    //Thêm post_tags
                    $tag_find_after_create = DB::table('tags')->where('slug', '=', $tag)->get();
                    $post_tag['tag_id'] = $tag_find_after_create[0]->id;
                    $post_tag['post_id'] = $data->id;
                    PostTag::create($post_tag);
                }
            }
            
        }
        
        //Thêm category
        $post_category['post_id'] = $data->id;
        $post_category['category_id'] = $category_id;
        PostCategory::create($post_category);

        $category = Category::find($category_id);
        $data->category = $category['name'];

        return $data;
    }

    public function detail(){
    	$slug = $_GET['title'];
    	$data = DB::table('posts')->where('slug', '=', $slug)->get();
    	
        $value = $data[0];
        $category_arr = array();
        $categories = DB::table('categories')
                    ->join('post_categories', 'categories.id', '=', 'post_categories.category_id')
                    ->where('post_categories.post_id', '=', $data[0]->id)
                    ->get();
        $category_arr = $categories;

        $tags = DB::table('tags')
                    ->join('post_tags', 'tags.id', '=', 'post_tags.tag_id')
                    ->where('post_tags.post_id', '=', $data[0]->id)
                    ->get();
        $user = User::find($value->user_id);
        $data[0]->author = $user['name'];

    	return view('admin/post/detail', ['data' => $data[0], 'category_arr' => $category_arr, 'tags' => $tags]);
    }
    public function findPost(){
        $slug = $_GET['title'];
        $data = DB::table('posts')->where('slug', '=', $slug)->get();
        $posts = Post::find($data[0]->id);
        $categories = $posts->categories;
        $posts['category'] = $categories[0]->id;
        $post_tags = DB::table('post_tags')->where('post_id', '=', $data[0]->id)->get();
        if(count($post_tags) > 0){
            foreach ($post_tags as $key => $post_tag) {
                $tag = Tag::find($post_tag->tag_id);
                $posts['tag'] .= $tag['name'].",";
            }
        }
        $posts['tag'] = trim($posts['tag'], ",");
        return $posts;
    }
    
    public function NotApprove(Request $req){
        $data = Post::find($req->id);
        $data->status = $req->status;

        if(isset($req->reason)){
            
            $reject['post_id'] = $req->id;
            $reject['reason'] = $req->reason;
            $reject['user_id'] = Auth::guard('admin')->user()->id;
            Reject::create($reject);

        }
        
        $data->save();

        
        
        return $data->id;
    }
    public function update(Request $req){
        $data = Post::find($req->id);
        $data->title = $req->title;
        $data->description = $req->description;
        $data->content = $req->content;
        
        $data->status = 1;
        if($req->thumbnail != "")
            $data->thumbnail = $req->thumbnail;

        $post_tag_old = DB::table('post_tags')->where('post_id', '=', $data->id)->get();
        $tag_old = array();
        if(count($post_tag_old) > 0){
            foreach ($post_tag_old as $key => $value) {
                //Lay tag
                $tag1 = Tag::find($value->tag_id);
                $tag_old[] = $tag1;
            }
        }
        //Lay tag
        $tagArr = $req->tag;
        $tags = array();
        $vt = strpos($tagArr, ',');
        for($i = 0; $i < strlen($tagArr); $i++){
            $length = strlen($tagArr);
            $str = substr($tagArr, 0, $vt);
            if($vt > 0)
                $tagArr = substr($tagArr, $vt+1, $length - strlen($str));
            $tags[] = $str;
            $vt = strpos($tagArr, ',');
        }
        $tags[] = $tagArr;

        //Them moi tag
        foreach ($tags as $tag) {
            if($tag != ""){
                $tagFind = DB::table('tags')->where('slug', '=', $tag)->get();
                //Nếu tồn tại tag này rồi thì thêm mới post_tags
                if(count($tagFind) > 0){
                    $tag_post_join = DB::table('post_tags')
                                        ->where('post_tags.tag_id', '=', $tagFind[0]->id)
                                        ->where('post_tags.post_id', '=', $data->id)
                                        ->get();
                    if(count($tag_post_join) <= 0){
                        $tag_id = $tagFind[0]->id;
                        $post_id = $data->id;
                        $post_tag['tag_id'] = $tag_id;
                        $post_tag['post_id'] = $post_id;
                        PostTag::create($post_tag);
                    }                }
                //Nếu chưa có thì thêm mới trong tags và cả post_tags
                else
                {
                    //Thêm tag
                    $tagAdd['name'] = $tag;
                    $tagAdd['slug'] = $tag;
                    $tagAdd = Tag::create($tagAdd);

                    //Thêm post_tags
                    $post_tag['tag_id'] = $tagAdd->id;
                    $post_tag['post_id'] = $data->id;
                    PostTag::create($post_tag);
                }
            }
            
        }


        $data->save();
        //Thay đổi category
        $category_id_new = $req->category_id_new;
        $category_old = DB::table('post_categories')
                    ->where('post_id', '=', $data->id)
                    ->get();
        if($category_old[0]->category_id != $category_id_new){
        	$categoryOld = PostCategory::find($category_old[0]->id);
        	$categoryOld->delete();
        	$category_new['post_id'] = $data->id;
        	$category_new['category_id'] = $category_id_new;
        	PostCategory::create($category_new);
        }

        $data->category = Category::find($category_id_new)['name'];
        foreach ($tag_old as $key => $value) {
            if(in_array($value['name'], $tags) == false){
                //xóa các bản ghi có tag_id = $value['id'] trong post_tags
                $post_tags = DB::table('post_tags')->where('tag_id', '=', $value['id'])->get();
                foreach ($post_tags as $post_tag) {
                    $post_tag_rs = PostTag::find($post_tag->id);
                    $post_tag_rs->delete();
                }
            }
        }

        if($data->user_id != 0){
        	$user = User::find($value->user_id);
        	$data->author = $user['name'];
        }elseif($data->admin_id != 0){
        	$admin = Admin::find($value->admin_id);
        	$data->author = $admin['name'];
        }
        else
        	$data->author = "master admin";

        
        return $data;
    }

    public function delete(Request $req){
        $data = Post::find($req->id);
        $id = $req->id;
        $data->delete();

        //Xóa các bản ghi trong post_tags
        $post_tags = DB::table('post_tags')->where('post_id', '=', $id)->get();
        foreach ($post_tags as $post_tag) {
            $post_tag_rs = PostTag::find($post_tag->id);
            $post_tag_rs->delete();
        }

        return $id;
    }

    public function upload_img(Request $request){
        $duoi = explode('.', $_FILES['file']['name']); // tách chuỗi khi gặp dấu .
        $duoi = $duoi[(count($duoi)-1)];//lấy ra đuôi file
        //Kiểm tra xem có phải file ảnh không
        if($duoi === 'jpg' || $duoi === 'png' || $duoi === 'gif' || $duoi === 'jpeg'){
            //tiến hành upload
            $url = 'img/' . $_FILES['file']['name'];
            if(move_uploaded_file($_FILES['file']['tmp_name'], $url)){
                //Nếu thành công
                echo $url;
            } else{ //nếu k thành công
                echo "Có lỗi"; //in ra thông báo
            }
        } else{ //nếu k phải file ảnh
            echo "File không phải ảnh"; //in ra thông báo
        }
    }
}
