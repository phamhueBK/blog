<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Admin;

class AdminController extends Controller
{
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function profile(){
        $posts = DB::table('posts')->where('user_id', '=', Auth::guard('admin')->user()->id)->get();
        return view('admin/admin/profile', ['post' => $posts]);
    }

    public function update(Request $request){
        $user = Admin::find(Auth::guard('admin')->user()->id);
        
        $user->name = $request->name;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->address = $request->address;
        $user->birthday = $request->birthday;
        if($request->avatar != "")
            $user->avatar = $request->avatar;

        $user->save();
        
        return $user;
    }
    public function getList(){
        $data = Admin::get();

        foreach ($data as $key => $value) {
            if($value['status'] == 0)
                $data[$key]['status'] = "Không hoạt động";
            else
                $data[$key]['status'] = "Đang hoạt động";
        }
        return view('admin/admin/index', ['data' => $data]);
    }

    public function nonActive(Request $request){
        $admin = Admin::find($request->id);
        $admin->status = $request->status;
        $admin->save();

        return $admin;
    }

    public function updateAdmin(Request $request){
        $admin = Admin::find($request->id);

        $admin->email = $request->email;
        $admin->name = $request->name;
        $admin->password = bcrypt($request->password);
        
        $admin->save();
        
        return $request;
    }

    public function create(Request $request){
        $admin['name'] = $request->name;
        $admin['email'] = $request->email;
        $admin['password'] = $request->password;
        $admin['status'] = 1;

        $admin = Admin::create($admin);

        return $admin;
    }

    public function findAdmin(){
        $id = $_GET['id'];
        $admin = Admin::find($id);
        return $admin;
    }
}
