<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'parent_id', 'left', 'right', 'slug',
    ];
    public function posts(){
    	return $this->belongsToMany('App\Post', 'post_categories', 'category_id', 'post_id');
    }
}
