<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Post;
use App\Category;
use DB;
use App\PostCategory;

class PostClaswer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clawer:post';//crawler

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //http://www.truyenngan.com.vn/truyen-ngan/truyen-ngan-ban.html
        //http://www.truyenngan.com.vn/truyen-ngan/truyen-ngan-song.html
        //http://www.truyenngan.com.vn/truyen-ngan/truyen-ngan-yeu.html
        //http://www.truyenngan.com.vn/truyen-ngan/truyen-ngan-gia-dinh.html
        //http://www.truyenngan.com.vn/truyen-ngan/truyen-ngan-tan-man.html
        //http://www.truyenngan.com.vn/truyen-ngan/ngu-ngon.html
        //http://www.truyenngan.com.vn/truyen-ngan/truyen-ngan-kinh-di.html
        //http://www.truyenngan.com.vn/truyen-blog/trai-nghiem-song.html
        //http://www.truyenngan.com.vn/truyen-blog/trai-nghiem-yeu.html
        //http://www.truyenngan.com.vn/sach-hay/tac-pham-du-luan.html
        //http://www.truyenngan.com.vn/sach-hay/nen-doc.html
        //http://www.truyenngan.com.vn/tam-su.html
        //http://www.truyenngan.com.vn/tieu-thuyet/truyen-ngon-tinh-hien-dai.html
        //http://www.truyenngan.com.vn/tieu-thuyet/xuyen-khong-co-dai-huyen-huyen.html
        //http://www.truyenngan.com.vn/tieu-thuyet/kiem-hiep.html
        //http://www.truyenngan.com.vn/tieu-thuyet/tieu-thuyet-phuong-tay.html
        //http://www.truyenngan.com.vn/tieu-thuyet/kinh-dien.html
        //http://www.truyenngan.com.vn/tieu-thuyet/truyen-kinh-di.html
        //http://www.truyenngan.com.vn/tieu-thuyet/tu-truyen.html
        $dom = file_get_html('http://www.truyenngan.com.vn/truyen-ngan/truyen-ngan-song.html');
        $posts_find = $dom->find('.container .box');

        $slug = 'truyen-ngan/truyen-ngan-song.html';
        $data = DB::table('categories')->where('slug', '=', $slug)->get();
        $category_id = $data[0]->id;

        $posts = array();
        foreach ($posts_find as $key => $value) {
            $post = array();
            $post['title'] = $value->find('h3 a', 0)->innertext;
            $post['slug'] = substr($value->find('a', 0)->href, 29);
            $post['thumbnail'] = $value->find('a img', 0)->src;
            $post['admin_id'] = 1;
            $post['user_id'] = 0;
            $post['status'] = 1;
            $post['views'] = 0;
            if(isset($value->find('div', 0)->innertext))
                $post['description'] = $value->find('div', 0)->innertext;
            
            $content_src = $value->find('a', 0)->href;
            $content_rs = file_get_html($content_src);
            $post['content'] = $content_rs->find('.maincontent', 0)->innertext;
            $posts[] = $post;
        }
        //dd($posts);
        foreach ($posts as $key => $value) {
            Post::create($value);
            $slug = $value['slug'];
            $data = DB::table('posts')->where('slug', '=', $slug)->get();
            $post_id = $data[0]->id;
            $post_category = array();
            $post_category['post_id'] = $post_id;
            $post_category['category_id'] = $category_id;
            PostCategory::create($post_category);
        }
    }
}
