<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Category;

class DBClawer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clawer:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lấy dữ liệu lưu vào db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dom = file_get_html('http://www.truyenngan.com.vn/');
        $category_names = $dom->find('.wrapper #wrap-mainmenu #mainmenu .nav li a');
        $categories = array();
        foreach ($category_names as $key => $value) {
            $category = array();
            $category['name'] = $value->innertext;
            $category['slug'] = substr($value->href, 29);
            $category['parent_id'] = 0;
            $category['parent_id'] = 0;
            $category['left'] = 0;
            $category['right'] = 0;
            $categories[] = $category;
        }
        //dd($categories);
        foreach ($categories as $key => $value) {
            Category::create($value);
        }
    }
}
