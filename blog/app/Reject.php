<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reject extends Model
{
    protected $fillable = [
        'reason', 'user_id', 'post_id',
    ];
}
