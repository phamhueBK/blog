<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'address', 'mobile', 'birthday', 'status',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
