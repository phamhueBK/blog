<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'description', 'content', 'thumbnail', 'status', 'views', 'user_id', 'slug',
    ];
    public function categories(){
    	return $this->belongsToMany('App\Category', 'post_categories', 'post_id', 'category_id');
    }
}
